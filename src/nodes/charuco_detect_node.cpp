#include <charuco_detect/charuco/charuco_detect.hpp>
#include <charuco_detect/conversion/pose_conversion.hpp>
#include <charuco_detect/visualization/draw_pose.hpp>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/TransformStamped.h>
#include <image_geometry/pinhole_camera_model.h>
#include <image_transport/image_transport.h>
#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Header.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2_ros/transform_broadcaster.h>

/*!
\brief Subscribes to a camera and publishes the charuco image and pose.
*/
class CharucoPosePublisher
{
private:
  // ROS stuff
  ros::NodeHandle node_handle;
  image_geometry::PinholeCameraModel camera;
  image_transport::ImageTransport image_transport;
  image_transport::CameraSubscriber cam_sub;
  image_transport::CameraPublisher cam_pub;
  tf2_ros::TransformBroadcaster tf_broadcaster;
  // Board to detect
  charuco_detect::Board board;
  // parameters
  std::string charuco_frame, camera_base_topic;
  int nx, ny;
  float marker_length, square_length;

public:
  CharucoPosePublisher() : image_transport(node_handle)
  {
    ros::NodeHandle pnh("~");
    pnh.param<std::string>("charuco_frame", charuco_frame, "charuco");
    // board
    pnh.param<int>("nx", nx, 5);
    pnh.param<int>("ny", ny, 7);
    pnh.param<float>("square_length", square_length, 0.034);
    pnh.param<float>("marker_length", marker_length, 0.024);
    board = charuco_detect::Board(nx, ny, square_length, marker_length);
    // camera
    pnh.param<std::string>("camera_base_topic", camera_base_topic, "/usb_cam/image_raw");
    cam_sub = image_transport.subscribeCamera(
        camera_base_topic, 1, &CharucoPosePublisher::camera_callback,
        this);
    cam_pub = image_transport.advertiseCamera("/charuco/image", 1);
  }

  void camera_callback(const sensor_msgs::ImageConstPtr &image_msg,
                       const sensor_msgs::CameraInfoConstPtr &info_msg)
  {
    // conversion using cv_bridge and image_geometry
    cv_bridge::CvImageConstPtr input_bridge;
    cv::Mat image;
    try
    {
      input_bridge = cv_bridge::toCvShare(image_msg);
      image = input_bridge->image;
    }
    catch (cv_bridge::Exception &e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    camera.fromCameraInfo(info_msg);
    board = charuco_detect::detect_board(image, board);
    // pose of board as tf
    auto estimated_pose = charuco_detect::charuco_pose(
        board, camera.intrinsicMatrix(), camera.distortionCoeffs());
    geometry_msgs::TransformStamped tf_stamped;
    tf_stamped.header.stamp = image_msg->header.stamp;
    tf_stamped.header.frame_id = info_msg->header.frame_id;
    tf_stamped.child_frame_id = charuco_frame;
    tf_stamped.transform = charuco_detect::convert_to_tf2(estimated_pose);
    // Publish the pose
    tf_broadcaster.sendTransform(tf_stamped);
    // only if subscribers for the image
    if (cam_pub.getNumSubscribers() > 0)
    {
      charuco_detect::draw_pose(image, camera.intrinsicMatrix(),
                                camera.distortionCoeffs(), estimated_pose,
                                board.board->getMarkerLength());
      std_msgs::Header header;
      auto pose_image_msg = cv_bridge::CvImage(info_msg->header,
                                               image_msg->encoding,
                                               image)
                                .toImageMsg();
      cam_pub.publish(pose_image_msg, info_msg);
    }
  }
};

int main(int argc, char **argv)
{
  // Init ROS
  ros::init(argc, argv, "charuco_detect_node", ros::init_options::AnonymousName);
  // Run the publisher
  CharucoPosePublisher pub;
  ros::spin();
  return EXIT_SUCCESS;
}
