#include <charuco_detect/visualization/draw_pose.hpp>
#include <opencv2/aruco.hpp>

namespace charuco_detect
{
void draw_pose(cv::InputOutputArray image,
                  const cv::InputArray &camera_matrix,
                  const cv::InputArray &dist_coeffs,
                  const Pose &pose,
                  float length)
{
  // Draw the axis
  cv::aruco::drawAxis(image, camera_matrix, dist_coeffs,
                      pose.rotation, pose.position, length);
}

} // namespace charuco_detect
